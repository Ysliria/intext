<?php

namespace App\Entity;

use App\Repository\ParticipantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParticipantRepository::class)
 */
class Participant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pro_status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $availability_status;

    /**
     * @ORM\Column(type="integer")
     */
    private $days_worked;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getProStatus(): ?string
    {
        return $this->pro_status;
    }

    public function setProStatus(string $pro_status): self
    {
        $this->pro_status = $pro_status;

        return $this;
    }

    public function getAvailabilityStatus(): ?string
    {
        return $this->availability_status;
    }

    public function setAvailabilityStatus(string $availability_status): self
    {
        $this->availability_status = $availability_status;

        return $this;
    }

    public function getDaysWorked(): ?int
    {
        return $this->days_worked;
    }

    public function setDaysWorked(int $days_worked): self
    {
        $this->days_worked = $days_worked;

        return $this;
    }
}
